<!--La carga de la hoja de estilos y del js luego habrá que quitarla cuando esto esté en el marco final-->
<html lang="es">
    <head>
        <link href="../css/bootstrap.css" rel="stylesheet">
    </head>
    <?php
//incluyo la función para conectarme a la BBDD
    include('../funciones.php');
    $mysqli = conectaLi();

//esto luego lo cambiamos por el usuario que esté logeado
    $id_usuario = 1;

    $query_random_enun = $mysqli->query("SELECT * FROM preguntas ORDER BY RAND() LIMIT 4");
    $num_preguntas = $query_random_enun->num_rows;

//guardo las respuestas en un array porque es más fácil trabajar luego con javascript
    $preguntas = array();
    for ($a = 0; $a < $num_preguntas; $a++) {
        $sqlrow = $query_random_enun->fetch_array();
        $preguntas [$a][0] = $sqlrow['enunciado'];
        $preguntas [$a][1] = $sqlrow['respuesta1'];
        $preguntas [$a][2] = $sqlrow['respuesta2'];
        $preguntas [$a][3] = $sqlrow['respuesta3'];
        $preguntas [$a][4] = $sqlrow['respuesta4'];
        $preguntas [$a][5] = $sqlrow['respuesta5'];
        $preguntas [$a][6] = $sqlrow['respuesta6'];
    }

    //a esta función le pasas la respuesta que quieres poner con php en la pantalla, y el número de respuesta
    //lo que hace es escribir el texto de la pregunta en la fila correspondiente
    function muestraRespuesta($respuesta, $numRespuesta) {
        if ($respuesta != "") {
            echo '<tr><td  id="respuesta' . $numRespuesta . '"><h4><input type=checkbox >  ' . $respuesta . '</h4></td></tr>';
        }
    }
    
    
    ?>
    <table class="table  table-condensed table-bordered" style="background-color: #999; color: whitesmoke;">
        <button class="btn btn-info" onclick="siguiente();">SIGUIENTE</button>
        <?php
        $a = 0;
        echo '<tr><td class="text-center" id="imagen"><img src="../img/' . $preguntas [$a][0] . '" style="width:800px"></td>';
        echo '</tr>';
        for ($i = 1; $i < 7; $i++) {
            muestraRespuesta($preguntas [$a][$i], $i);
        }
        ?>
    </table>


<!--esta carga de script de jquery y bootstrap hay que borrarla cuando pongamos este código dentro del marco final-->
    <script src="../js/jquery-3.1.1.min.js"></script>
    <script src="../js/bootstrap.min.js"></script>
    <script>
            //paso las preguntas de php a javascript
            var preguntas = <?php echo json_encode($preguntas); ?>;
            var max_preguntas = <?php echo json_encode($num_preguntas); ?>;
            var num_pregunta = 0;
            
            //La función siguiente se encarga de manejar lo que pasa cuando se pulsa el botón siguiente
            //tiene que: 
            //  cambiar la imagen
            //  cambiar las preguntas, y si están en blanco no ponerlas
            //  guardar las respuestas que ha dado el usuario en la base de datos
            //  chequear si ha terminado la partida, si quiere pausar, recuperar un test empezado etc
            
            function siguiente() {
                num_pregunta++;
                //pone la imagen correspondiente
                $('#imagen').html('<img src="../img/' + preguntas[num_pregunta][0] + '" style="width:800px">');
                //pone cada respuesta en su sitio. Si está en blanco, lo borra por si hubiera otra respuesta anterior
                for (var i = 1; i < 7; i++) {
                    if (preguntas[num_pregunta][i] != "") {
                        $('#respuesta' + i).html('<tr><td  id="respuesta' + i + '"><h4><input type=checkbox >  ' + preguntas[num_pregunta][i] + '</h4></td></tr>');
                    }
                    else {
                        $('#respuesta' + i).html('');
                    }
                }
            }



    </script>


